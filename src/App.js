import React, { useState } from "react";
import "./App.css";

import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import Grid from '@mui/material/Grid';
import Nav from "./components/Nav.js";
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Web3 from "web3";

import { hoaBallot } from "./abi/abi";
import { makeStyles } from "@material-ui/core/styles";
import { styled } from '@mui/material/styles';

const web3 = new Web3(Web3.givenProvider);
const contractAddress = "0x2981d347e288E2A4040a3C17c7e5985422e3cAf2";
const storageContract = new web3.eth.Contract(hoaBallot, contractAddress);
const gasMultiplier = 1.5;

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    fontSize: 14,
    fontWeight: 'bold'
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14
  },
}));

function App() {
  const classes = useStyles();
  const [newCandidateName, setNewCandidateName] = useState("");
  const [account, setAccount] = useState("");
  const [owner, setOwner] = useState("");
  const [candidates, updateCandidates] = useState([]);
  const [winner, setWinner] = useState("unknown candidate");
  const [waiting, setWaiting] = useState(false);

  const loadAccount = async(useSpinner) => {
    if (useSpinner) {
      setWaiting(true);
    }

    const web3 = new Web3(Web3.givenProvider || "http://localhost:8080");
    const accounts = await web3.eth.getAccounts();
    setAccount(accounts[0]);

    if (useSpinner) {
      setWaiting(false);
    }
  }

  const getOwner = async (useSpinner) => {
    if (useSpinner) {
      setWaiting(true);
    }

    const owner = await storageContract.methods.owner().call();
    setOwner(owner);

    if (useSpinner) {
      setWaiting(false);
    }
  };

  const getCandidates = async (useSpinner) => {
    if (useSpinner) {
      setWaiting(true);
    }

    const candidates = await storageContract.methods.getCandidates().call();

    updateCandidates(candidates);

    await determineWinner();

    if (useSpinner) {
      setWaiting(false);
    }
  };

  const determineWinner = async () => {
    const winner = await storageContract.methods.getWinner().call();

    if (winner && winner.name) {
      setWinner(winner.name);
    } else {
      setWinner("<unknown candidate>")
    }
  }

  const vote = async (candidate) => {
    setWaiting(true);

    const gas = (await storageContract.methods.addVoteByName(candidate).estimateGas({
      data: candidate,
      from: account
    })) * gasMultiplier;

    let gasAsInt = gas.toFixed(0);

    await storageContract.methods.addVoteByName(candidate).send({
      from: account,
      data: candidate,
      gasAsInt,
    });

    await getCandidates(false);

    setWaiting(false);
  }

  const removeCandidate = async (candidate) => {
    setWaiting(true);

    const gas = (await storageContract.methods.removeCandidate(candidate).estimateGas({
      data: candidate,
      from: account
    })) * gasMultiplier;

    let gasAsInt = gas.toFixed(0);

    await storageContract.methods.removeCandidate(candidate).send({
      from: account,
      data: candidate,
      gasAsInt,
    });

    await getCandidates(false);

    setWaiting(false);
  }

  const addCandidate = async () => {
    setWaiting(true);

    const gas = (await storageContract.methods.addCandidate(newCandidateName).estimateGas({
      data: newCandidateName,
      from: account
    })) * gasMultiplier;

    let gasAsInt = gas.toFixed(0);

    await storageContract.methods.addCandidate(newCandidateName).send({
      from: account,
      data: newCandidateName,
      gasAsInt,
    });

    await getCandidates(false);

    setWaiting(false);
  }

  React.useEffect(() => {
    setWaiting(true);
    getOwner(false).then(r => {
      loadAccount(false).then(r => {
        getCandidates(false).then(r => {
          setWaiting(false);
        });
      });
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  return (
      <div className={classes.root}>
        <Nav />
        <div className="main">
          <div className="card">
            <Typography variant="h3">
              HOABallot
            </Typography>

            {(owner && owner.length > 0) && (
                <div className="paddingBelow">
                  <Typography variant="caption" >
                    This ballot is owned by: {owner}
                  </Typography>
                </div>
            )}

            {waiting && (
                <div className="spinnerArea" >
                  <CircularProgress />
                  <Typography gutterBottom>
                    Processing Request ... please wait
                  </Typography>
                </div>
            )}

            {(owner && owner.length > 0 && account && account.length > 0 && owner === account) && (
                <div className="ownerActions generalPadding">
                  <Grid container spacing={3}>
                    <Grid item xs={12}>
                      <Typography variant="h6" gutterBottom>
                        Ballot Owner Actions
                      </Typography>
                    </Grid>
                    <Grid item xs={6} sm={6}>
                      <TextField id="newCandidateName"
                                 value={newCandidateName}
                                 label="Candidate Name"
                                 variant="outlined"
                                 onChange={event => {
                                   const { value } = event.target;
                                   setNewCandidateName(value);
                                 }}
                      />
                    </Grid>
                    <Grid item xs={6} sm={6}>
                      <Button
                          id="addCandidateButton"
                          className="button"
                          variant="contained"
                          color="primary"
                          type="button"
                          size="large"
                          onClick={addCandidate}>Add New Candidate</Button>
                    </Grid>
                  </Grid>
                </div>
            )}

            <Typography variant="h5" gutterBottom className="generalPadding">
              Candidates
            </Typography>

            {(!candidates || candidates.length === 0) && (
                <div>
                  <div className="paddingBelow">
                    <Typography variant="normal">
                      No candidates current exist.
                    </Typography>
                  </div>
                  <div>
                    <Typography variant="normal" gutterBottom>
                      Ballot owner must use the <strong>ADD NEW CANDIDATE</strong> button to add candidates.
                    </Typography>
                  </div>
                </div>
            )}

            {(candidates && candidates.length > 0) && (
                <div>
                  <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="customized table">
                      <TableHead>
                        <TableRow>
                          <StyledTableCell>Candidate Name</StyledTableCell>
                          <StyledTableCell align="right">Votes</StyledTableCell>
                          <StyledTableCell align="center">Actions</StyledTableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {candidates.map((row) => (
                            <TableRow
                                key={row.name}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                              <TableCell component="th" scope="row">
                                {row.name}
                              </TableCell>
                              <TableCell align="right">{row.votes}</TableCell>
                              <TableCell align="center">
                                <Button
                                    color="success"
                                    variant="contained"
                                    onClick={() => {
                                      vote(row.name);
                                    }}
                                >
                                    Vote
                                </Button> &nbsp;
                                {(owner && owner.length > 0 && account && account.length > 0 && owner === account) &&
                                  <Button
                                      color="error"
                                      variant="contained"
                                      onClick={() => {
                                        removeCandidate(row.name);
                                      }}
                                  >
                                    Remove Candidate
                                  </Button>
                                }
                              </TableCell>
                            </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                  <div className="generalPadding">
                    <Typography variant="normal" gutterBottom>
                      {winner} is winning the election.
                    </Typography>
                  </div>

                </div>
            )}
          </div>
        </div>
      </div>
  );
}

export default App;
