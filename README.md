# `hoa-ballot-client` Repository

> The `hoa-ballot-client` repository is simple web3 Dapp example built upon [React](https://reactjs.org/),
> [web3](https://web3js.readthedocs.io/en/v1.3.0/) and [MetaMask](https://metamask.io/) dependencies - along with the smart
> contract established in the [`hoa-ballot-contract`](https://gitlab.com/johnjvester/hoa-ballot-contract) repository.

## Publications

This repository is related to a DZone.com publication:

* [Diving Deep into Smart Contracts](https://dzone.com/articles/diving-deep-into-smart-contracts)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Using This Repository

At a high level, the following steps are required to utilize this repository:

1. Migrate the smart contract within the `hoa-ballot-contract` repository
2. Update the `App.js` and the `contractAddress` with the correct value
3. `npm install` or `npm ci`
4. `yarn start` or `npm start`

Below, is a short demonstration of the React app in action:

[YouTube - Demonstration](https://youtu.be/r5mROTY8efw)

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
